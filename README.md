Run nodemon (what gitlab development kit uses)
`yarn dev-server`

Run node with --inspect flag
`yarn node-inspect`
View trace in chrome://traces

Follow steps from [this comment](https://github.com/webpack/webpack/issues/6929#issuecomment-403441611) to search for usage and memory leaks
